Сборка происходит в контейнерах. Репозиторий поднят в gcp.

Установка репозитория в bionic:
```
curl https://europe-north1-apt.pkg.dev/doc/repo-signing-key.gpg | apt-key add -
```
```
echo "deb https://europe-north1-apt.pkg.dev/projects/testtask-advsolutions testtask main" | tee -a /etc/apt/sources.list.d/artifact-registry.list
```
